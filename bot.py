from binance.client import Client
from binance.enums import *
import binance.exceptions as binex
import websocket, json, api_cfg, talib, numpy, logging, logging.config, math, time
from logging.handlers import TimedRotatingFileHandler
from datetime import datetime
from pathlib import Path
import threading
import requests.exceptions as req_exc

RING_SIZE = 100
RSI_PERIOD = 14
RSI_OVERBOUGHT = 70
RSI_OVERSOLD = 30
ATR_PERIOD = 14
ATR_MULTIPLIER = 3.0

class TradingBot(threading.Thread):

    FIAT_CURRENCY = "EUR"

    def __init__(self, trade_asset, how_much_buy_in_euros):
        
        self.trade_asset = trade_asset
        self.trade_symbol = trade_asset + self.FIAT_CURRENCY
        self.buy_volume = how_much_buy_in_euros

        # logging stuff
        logging.config.fileConfig('logging.conf')
        self.logger = logging.getLogger('abbie')

        self.keep_trying = True
        self.is_running = False
        self.first_run = True
        self.init = True

        self.client = Client(api_cfg.API_KEY, api_cfg.SECRET_KEY, {"timeout": 10})

        # get symbol info, for round down precision
        self.symbol_info = self.client.get_symbol_info(self.trade_symbol)
        for filter in self.symbol_info['filters']:
                if filter['filterType'] == 'LOT_SIZE':
                        self.step_size = filter['stepSize']
                        break
        
        try:
            self.PRECISION = int(round(-math.log(float(self.step_size), 10), 0))
        except ValueError as e:
            self.logger.error(self.trade_asset + " - wrong value - {}".format(e))
            return

        # check if user has cryptocurrency (more than 20 eur)
        balance = self.client.get_asset_balance(asset=self.trade_asset)
        curr_price = self.client.get_avg_price(symbol=self.trade_symbol)
        try:
            balance_eur = float(balance['free']) * float(curr_price['price'])
            if balance_eur > 20.0:
                self.in_position = True
                self.logger.info(self.trade_asset + " - balance: " + balance['free'] + " | balance_euro: " + str(balance_eur) + " | IN POSITION")
            else:
                self.in_position = False
                self.logger.info(self.trade_asset + " - balance: " + balance['free'] + " | balance_euro: " + str(balance_eur) + " | NOT IN POSITION")
        except ValueError as e:
            self.logger.error(self.trade_asset + " - wrong value - {}".format(e))
            return
        except Exception as e:
            self.logger.error(self.trade_asset + " - an exception occured - {}".format(e))
            return # ???

        historical_candles = self.client.get_klines(symbol=self.trade_symbol, interval=KLINE_INTERVAL_15MINUTE, limit=100)
        historical_closes = []
        historical_highs = []
        historical_lows = []
        for candle in historical_candles:
            # 4 is close price
            historical_closes.append(float(candle[4]))
            historical_highs.append(float(candle[2]))
            historical_lows.append(float(candle[3]))
        self.closes = numpy.array(historical_closes)
        self.highs = numpy.array(historical_highs)
        self.lows = numpy.array(historical_lows)
        # all prices not closes
        self.prices = []

        self.socket = "wss://stream.binance.com:9443/ws/" + str.lower(self.trade_asset) + "eur@kline_15m"   
        self.ws = websocket.WebSocketApp(self.socket, on_open=self.on_open, on_close=self.on_close, on_message=self.on_message)
        threading.Thread.__init__(self, target=self.keep_making_money, name=self.trade_asset, daemon=True)


    def keep_making_money(self):
        self.logger.info(self.trade_asset + " - MAKE SOME MF MONEY")
        while self.keep_trying:
            if not self.is_running:
                if self.first_run:
                    self.first_run = False
                else:
                    self.logger.info(self.trade_asset + " - retrying connection...")
                self.connect_websocket()
                time.sleep(1)
            # consider change, delay closing
            time.sleep(60)


    def connect_websocket(self):
        self.wst = threading.Thread(target=self.ws.run_forever, name=self.trade_asset, daemon=True)
        self.wst.start()


    def close_ws(self):
        self.keep_trying = False
        self.ws.keep_running = False


    def on_open(self, ws):
        self.logger.info(self.trade_asset + " - opened connection")
        self.is_running = True


    def on_close(self, ws):
        self.logger.info(self.trade_asset + " - closed connection")
        self.is_running = False


    def on_message(self, ws, message):
        # self.logger.info(self.trade_asset + ": received message")
        json_message = json.loads(message)
        candle = json_message['k']
        print(self.trade_asset + " - last price: " + candle['c'])
        try:
        # all prices - for retrying delayed orders
            self.prices.append(float(candle['c']))
            if len(self.prices) > 100:
                self.prices = self.prices[-100:]
        except Exception as e:
            self.logger.error(self.trade_asset + " - an exception occured - {}".format(e))

        # is candle closed
        if candle['x']:
            # add new close
            try:
                self.closes = numpy.append(self.closes, float(candle['c']))
                self.highs = numpy.append(self.highs, float(candle['h']))
                self.lows = numpy.append(self.lows, float(candle['l']))
                # delete old closes, max len = 100
                if len(self.closes) > 100:
                    self.closes = self.closes[-100:]
                if len(self.highs) > 100:
                    self.highs = self.highs[-100:]
                if len(self.lows) > 100:
                    self.lows = self.lows[-100:]
                self.rsi = talib.RSI(self.closes, RSI_PERIOD)
                self.atr = talib.ATR(self.highs, self.lows, self.closes, timeperiod=ATR_PERIOD)
                self.logger.info(self.trade_asset + " - candle closed at " + str(candle['c']) + " | last rsi = " + str(self.rsi[-1]))

                if self.init:
                    self.pstop = self.closes[-1] - (self.atr[-1] * ATR_MULTIPLIER)
                    self.init = False
            except Exception as e:
                self.logger.error(self.trade_asset + " - candle parsing/indicators exception occured - {}".format(e))

            # self.logger.info(self.trade_asset +  ": last rsi = " + str(self.rsi[-1]))

            # SMA1 = bt.indicators.SMA(closes_array, period=5)
            # SMA2 = bt.indicators.SMA(closes_array, period=10)
            # SMAcross = bt.indicators.CrossOver(SMA1, SMA2)

            if not self.in_position:
                if self.rsi[-1] < RSI_OVERSOLD:
                    self.logger.info(self.trade_asset + " - imma buy some shit")
                    #buy
                    try:
                        if self.buy():
                            self.in_position = True
                    except Exception as e:
                        self.logger.error(self.trade_asset + " - buy exception occured - {}".format(e))
                self.pstop = self.closes[-1] - (self.atr[-1] * ATR_MULTIPLIER)
            else:
                # if self.rsi[-1] > RSI_OVERBOUGHT:
                if self.closes[-1] < self.pstop:
                    self.logger.info(self.trade_asset + " - imma sellin dis shit")
                    #sell
                    try:
                        if self.sell():
                            self.in_position = False
                    except Exception as e:
                        self.logger.error(self.trade_asset + " - sell exception occured - {}".format(e))
                else:
                    self.pstop = max(self.pstop, (self.closes[-1] - (self.atr[-1] * ATR_MULTIPLIER)))


    def place_order(self, side, quantity, symbol, order_type):
        try:
            self.logger.info("sending order")
            order = self.client.create_order(symbol=symbol, side=side, type=order_type, quantity=quantity)
            self.logger.info(order)
        except Exception as e:
            self.logger.error(self.trade_asset + " - place order exception occured - {}".format(e))
            return False
        return True


    def buy(self):
        try:
            # how_much_buy_in_euros / last_known_price -> commission -> round_down
            quantity = self.round_decimals_down(((self.buy_volume / self.closes[-1]) * 0.999), self.PRECISION)
            self.logger.info("BUY ORDER - " + str(quantity) + " " + self.trade_asset)
            self.client.order_market_buy(symbol=self.trade_symbol, quantity=quantity)
            # todo: what if not enough money
        # except binex.APIError as ae:
        #     quantity = self.round_decimals_down(((self.buy_volume / self.closes[-1]) * 0.995), self.PRECISION)
        #     self.logger.info("BUY ORDER - " + str(quantity) + " " + self.trade_asset)
        #     self.client.order_market_buy(symbol=self.trade_symbol, quantity=quantity)
        #     self.logger.error(self.trade_asset + " - an API exception occured - {}".format(ae))
        except req_exc.ReadTimeout as rt:
            self.logger.error(self.trade_asset + " - read timeout exceeded - {}".format(rt))
            tries_left = 5
            success = False
            while tries_left > 0 and success == False:
                try:
                    self.logger.error(self.trade_asset + " - retrying buy order... try: - " + str(tries_left))
                    quantity = self.round_decimals_down(((self.buy_volume / self.prices[-1]) * 0.999), self.PRECISION)
                    self.logger.info("BUY ORDER - " + str(quantity) + " " + self.trade_asset)
                    self.client.order_market_buy(symbol=self.trade_symbol, quantity=quantity)
                    success = True
                except req_exc.ReadTimeout as rt2:
                    tries_left = tries_left - 1
            if success:
                return True
            else:
                return False
        return True #self.place_order(self, side=SIDE_BUY, symbol=self.trade_symbol, order_type=ORDER_TYPE_MARKET, quantity=quantity)


    def sell(self):
        try:
            # asset_balance -> commission -> round_down
            balance = self.client.get_asset_balance(asset=self.trade_asset)
            quantity = self.round_decimals_down((float(balance['free']) * 0.999), self.PRECISION)
            price = quantity * self.closes[-1]
            self.logger.info("SELL ORDER - " + str(quantity) + " " + self.trade_asset + " for " + str(price) + " EUR")
            self.client.order_market_sell(symbol=self.trade_symbol, quantity=quantity)
        # except binex.APIError as ae:
        #     balance = self.client.get_asset_balance(asset=self.trade_asset)
        #     quantity = self.round_decimals_down((float(balance['free']) * 0.995), self.PRECISION)
        #     price = quantity * self.closes[-1]
        #     self.logger.info("SELL ORDER - " + str(quantity) + " " + self.trade_asset + " for " + str(price) + " EUR")
        #     self.client.order_market_sell(symbol=self.trade_symbol, quantity=quantity)
        #     self.logger.error(self.trade_asset + " - an API exception occured - {}".format(ae))
        except ValueError as ve:
            self.logger.error(self.trade_asset + " - value exception occured - {}".format(ve))
        except req_exc.ReadTimeout as rt:
            self.logger.error(self.trade_asset + " - read timeout exceeded - {}".format(rt))
            tries_left = 5
            success = False
            while tries_left > 0 and success == False:
                try:
                    self.logger.error(self.trade_asset + " - retrying sell order... try: - " + str(tries_left))
                    balance = self.client.get_asset_balance(asset=self.trade_asset)
                    quantity = self.round_decimals_down((float(balance['free']) * 0.999), self.PRECISION)
                    price = quantity * self.prices[-1]
                    self.logger.info("SELL ORDER - " + str(quantity) + " " + self.trade_asset + " for " + str(price) + " EUR")
                    self.client.order_market_sell(symbol=self.trade_symbol, quantity=quantity)
                    success = True
                except req_exc.ReadTimeout as rt2:
                    tries_left = tries_left - 1
            if success:
                return True
            else:
                return False
        return True #self.place_order(self, side=SIDE_SELL, symbol=self.trade_symbol, order_type=ORDER_TYPE_MARKET, quantity=quantity)


    # ----------------------

    def round_decimals_down(self, number:float, decimals:int=2):
        """
        Returns a value rounded down to a specific number of decimal places.
        """
        if not isinstance(decimals, int):
            raise TypeError("decimal places must be an integer")
        elif decimals < 0:
            raise ValueError("decimal places has to be 0 or more")
        elif decimals == 0:
            return math.floor(number)

        factor = 10 ** decimals
        return math.floor(number * factor) / factor

