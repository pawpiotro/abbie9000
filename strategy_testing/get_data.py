from binance.exceptions import BinanceRequestException, BinanceAPIException
from binance.client import Client
from datetime import datetime
from pathlib import Path
import csv, sys
sys.path.insert(0,'..')
import api_cfg


# SYMBOL='BTCEUR'
# SYMBOL='DOGEEUR'
# SYMBOL='BNBEUR'
# SYMBOL='ETHEUR'
# SYMBOL='LTCEUR'
# SYMBOL='XLMEUR'
# SYMBOL='BCHEUR'
# SYMBOL='DOTEUR'
# SYMBOL='ADAEUR'

# INTERVAL=Client.KLINE_INTERVAL_1MINUTE
# INTERVAL=Client.KLINE_INTERVAL_3MINUTE
# INTERVAL=Client.KLINE_INTERVAL_5MINUTE
INTERVAL=Client.KLINE_INTERVAL_15MINUTE
# INTERVAL=Client.KLINE_INTERVAL_30MINUTE
# INTERVAL=Client.KLINE_INTERVAL_1HOUR
# INTERVAL=Client.KLINE_INTERVAL_2HOUR
# INTERVAL=Client.KLINE_INTERVAL_4HOUR
# INTERVAL=Client.KLINE_INTERVAL_6HOUR
# INTERVAL=Client.KLINE_INTERVAL_8HOUR
# INTERVAL=Client.KLINE_INTERVAL_12HOUR
# INTERVAL=Client.KLINE_INTERVAL_1DAY


def save_candles(symbol, candlesticks, start_date, end_date, interval=INTERVAL):
    if start_date is None or end_date is None:
        now = datetime.now()
        curr_date = now.strftime("%Y_%m_%d-%H_%M_%S")
        Path("./data").mkdir(parents=True, exist_ok=True)
        save_path = Path('./data/' + symbol + '_' + interval + '_' + curr_date + '.csv')
    else:
        start_date = "_".join(start_date.replace(',','').split(" "))
        end_date = "_".join(end_date.replace(',','').split(" "))
        Path("./data").mkdir(parents=True, exist_ok=True)
        save_path = Path('./data/' + symbol + '_' + '_' + interval + '_' + start_date + '_' + end_date + '.csv')
    csvfile = open(save_path, 'w', newline='') 
    candlestick_writer = csv.writer(csvfile, delimiter=',')
    for candlestick in  candlesticks:
            candlestick[0] = candlestick[0] / 1000
            candlestick_writer.writerow(candlestick)
    csvfile.close()
    print(save_path)


def get_data(symbol, interval=INTERVAL):
    client = Client(api_cfg.API_KEY, api_cfg.SECRET_KEY)

    try:
        # latest
        candlesticks = client.get_klines(symbol=symbol, interval=interval, limit=1000)
        start_date = None
        end_date = None

        # historical
        # start_date = "1 Jan, 2020"
        # end_date = "12 Jul, 2020"
        # candlesticks = client.get_historical_klines(symbol=symbol, interval=interval, limit=1000, start_str=start_date, end_str=end_date)

        save_candles(symbol, candlesticks, start_date, end_date)
        print("Latest data for " + symbol + " downloaded succesfully.")
    except BinanceRequestException as bre:
        print(bre)
        sys.exit()
    except BinanceAPIException as bapie:
        print(bapie)
        sys.exit()


get_data('BTCEUR')
# get_data('DOGEEUR')
get_data('BNBEUR')
get_data('LTCEUR')
get_data('ETHEUR')
get_data('XLMEUR')
get_data('DOTEUR')
get_data('ADAEUR')