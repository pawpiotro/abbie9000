import backtrader as bt
from strategies import RSIStrategy, MACDStrategy, SMAStrategy, RSISMAStrategy, RSIMACDStrategy, RSIATRStrategy
from pathlib import Path


# FILENAME = 'BTCEUR_15m_2021_06_23-08_07_19.csv'
# DIR = './data/'
# PATH = Path(DIR+FILENAME)

# PATH=Path('./data/BTCEUR_15m_2021_03_25-14_33_08.csv')
# PATH=Path('./data/BTCEUR_15m_2021_03_25-21_37_11.csv')
# PATH=Path('./data/BTCEUR_15m_2021_03_31-15_36_07.csv')
# PATH=Path('./data/BTCEUR_2021_03_16-14_47_27.csv')
# PATH=Path('./data/BTCEUR_2021_03_21-15_56_17.csv')
# PATH=Path('./data/BTCEUR_15m_2021_04_07-14_18_50.csv')
# PATH=Path('./data/BTCEUR_15m_2021_04_12-21_24_53.csv')
# PATH=Path('./data/BTCEUR_15m_2021_04_16-00_35_50.csv')
# PATH=Path('./data/BTCEUR_15m_2021_04_23-12_13_53.csv')
# PATH=Path('./data/BTCEUR_15m_2021_05_28-09_59_30.csv')
# PATH=Path('./data/BTCEUR_15m_2021_05_29-13_20_59.csv')
# PATH=Path('./data/BTCEUR_15m_2021_05_31-10_33_41.csv')
# PATH=Path('./data/BTCEUR_15m_2021_06_19-00_11_07.csv')
# PATH=Path('./data/BTCEUR_15m_2021_06_19-19_12_48.csv')
# PATH=Path('./data/BTCEUR_15m_2021_06_23-08_07_19.csv')
# PATH=Path('./data/BTCEUR_15m_2021_06_24-10_20_48.csv')
# PATH=Path('./data/BTCEUR_15m_2021_06_25-10_43_14.csv')
# PATH=Path('./data/BTCEUR_15m_2021_06_25-10_43_14.csv')


PATHS = [
    # Path('./data/BTCEUR_15m_2021_03_25-14_33_08.csv'),
    # Path('./data/BTCEUR_15m_2021_03_25-21_37_11.csv'),
    # Path('./data/BTCEUR_15m_2021_03_31-15_36_07.csv'),
    # Path('./data/BTCEUR_2021_03_16-14_47_27.csv'),
    # Path('./data/BTCEUR_2021_03_21-15_56_17.csv'),
    # Path('./data/BTCEUR_15m_2021_04_07-14_18_50.csv'),
    # Path('./data/BTCEUR_15m_2021_04_12-21_24_53.csv'),
    # Path('./data/BTCEUR_15m_2021_04_16-00_35_50.csv'),
    # Path('./data/BTCEUR_15m_2021_04_23-12_13_53.csv'),
    Path('./data/BTCEUR_15m_2021_05_28-09_59_30.csv'),
    Path('./data/BTCEUR_15m_2021_05_29-13_20_59.csv'),
    Path('./data/BTCEUR_15m_2021_06_19-00_11_07.csv'),
    Path('./data/BTCEUR_15m_2021_06_19-19_12_48.csv'),
    Path('./data/BTCEUR_15m_2021_06_23-08_07_19.csv'),
    Path('./data/BTCEUR_15m_2021_05_31-10_33_41.csv'),
    Path('./data/BTCEUR_15m_2021_06_24-10_20_48.csv'),
    Path('./data/BTCEUR_15m_2021_06_25-10_43_14.csv'),
    Path('./data/LTCEUR_15m_2021_07_03-18_41_17.csv')
]

CASH = 8000
COMPRESSION = 15
TIMEFRAME = bt.TimeFrame.Minutes


def backtest_strategies(strategy, PATH):
    cerebro = bt.Cerebro()
    cerebro.broker.set_cash(CASH)
    data = bt.feeds.GenericCSVData(dataname=PATH, dtformat=2, compression=COMPRESSION, timeframe=TIMEFRAME)
    cerebro.adddata(data)
    cerebro.addstrategy(strategy)
    cerebro.run()
    return cerebro.broker.getvalue()


# single, plot
def backtest_strategy(strategy, PATH):
    cerebro = bt.Cerebro()
    cerebro.broker.set_cash(CASH)
    data = bt.feeds.GenericCSVData(dataname=PATH, dtformat=2, compression=COMPRESSION, timeframe=TIMEFRAME)
    cerebro.adddata(data)
    cerebro.addstrategy(strategy)
    cerebro.run()
    cerebro.plot()
    return cerebro.broker.getvalue()


def score_strategies():
    scores = [0,0,0,0,0,0]
    cur_values = [0,0,0,0,0,0]
    profits = [0,0,0,0,0,0]
    for p in PATHS:
        cur_values[0] = backtest_strategies(SMAStrategy, p)
        print("SMA: " + str(cur_values[0]))
        profits[0] = profits[0] + (cur_values[0] - CASH)
        cur_values[1] = backtest_strategies(MACDStrategy, p)
        print("MACD: " + str(cur_values[1]))
        profits[1] = profits[1] + (cur_values[1] - CASH)
        cur_values[2] = backtest_strategies(RSIStrategy, p)
        print("RSI: " + str(cur_values[2]))
        profits[2] = profits[2] + (cur_values[2] - CASH)
        cur_values[3] = backtest_strategies(RSISMAStrategy, p)
        print("RSISMA: " + str(cur_values[3]))
        profits[3] = profits[3] + (cur_values[3] - CASH)
        cur_values[4] = backtest_strategies(RSIMACDStrategy, p)
        print("RSIMACD: " + str(cur_values[4]))
        profits[4] = profits[4] + (cur_values[4] - CASH)
        cur_values[5] = backtest_strategies(RSIATRStrategy, p)
        print("RSIATR: " + str(cur_values[5]))
        profits[5] = profits[5] + (cur_values[5] - CASH)

        for i in range(0,6):
            if cur_values[i] == max(cur_values):
                tmp_max = i
        scores[tmp_max] = scores[tmp_max] + 1
        print("------------------------")
        
    print("SMA MACD RSI RSISMA RSIMACD RSIATR")
    print(scores)
    print(profits)


def all_data_test(strategy):
    for p in PATHS:
        backtest_strategy(strategy, p)

def main():
    score_strategies()

    # backtest_strategy(RSIStrategy, PATH)
    # backtest_strategy(MACDStrategy, PATH)
    # backtest_strategy(SMAStrategy, PATH)
    # backtest_strategy(MyStrategy, PATH)

    # all_data_test(RSIATRStrategy)

main()