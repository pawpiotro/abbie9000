import backtrader as bt
import talib


BUY_SIZE = 0.02

class RSIStrategy(bt.Strategy):
    def __init__(self):
        self.rsi = bt.indicators.RSI(self.data, period=14)
        #bt.indicators.RSI(self.data, period=14)
        #bt.talib.RSI(self.data, period=14)

    def next(self):
        if self.rsi < 30 and not self.position:
            self.buy(size=BUY_SIZE)
        
        if self.rsi > 70 and self.position:
            self.close()


class MACDStrategy(bt.Strategy):
    params = (
        # Standard MACD Parameters
        ('macd1', 12),
        ('macd2', 26),
        ('macdsig', 9),
        ('atrperiod', 14),  # ATR Period (standard)
        ('atrdist', 3),   # ATR distance for stop price
        ('smaperiod', 15),  # SMA Period (pretty standard)
        ('dirperiod', 5),  # Lookback period to consider SMA trend direction
    )


    def notify_order(self, order):
        if order.status == order.Completed:
            pass

        if not order.alive():
            self.order = None  # indicate no order is pending

    def __init__(self):
        self.macd = bt.indicators.MACD(self.data,
                                       period_me1=self.p.macd1,
                                       period_me2=self.p.macd2,
                                       period_signal=self.p.macdsig)

        # Cross of macd.macd and macd.signal
        self.mcross = bt.indicators.CrossOver(self.macd.macd, self.macd.signal)

        # To set the stop price
        self.atr = bt.indicators.ATR(self.data, period=self.p.atrperiod)

        # Control market trend
        self.sma = bt.indicators.SMA(self.data, period=self.p.smaperiod)
        self.smadir = self.sma - self.sma(-self.p.dirperiod)

    def start(self):
        self.order = None  # sentinel to avoid operrations on pending order

    def next(self):
        if self.order:
            return  # pending order execution

        if not self.position:  # not in the market
            if self.mcross[0] > 0.0 and self.smadir < 0.0:
                self.order = self.buy(size=BUY_SIZE)
                pdist = self.atr[0] * self.p.atrdist
                self.pstop = self.data.close[0] - pdist

        else:  # in the market
            pclose = self.data.close[0]
            pstop = self.pstop

            if pclose < pstop:
                self.close()  # stop met - get out
            else:
                pdist = self.atr[0] * self.p.atrdist
                # Update only if greater than
                self.pstop = max(pstop, pclose - pdist)


class SMAStrategy(bt.Strategy):
    def __init__(self):
        self.sma1 = bt.indicators.SMA(self.data, period=10)
        self.sma2 = bt.indicators.SMA(self.data, period=50)
        # self.sma3 = bt.indicators.SMA(self.data, period=20)

        self.mcross = bt.indicators.CrossOver(self.sma1, self.sma2)
        # self.mcross2 = bt.indicators.CrossOver(self.sma2, self.sma1)
        

    def next(self):
        if not self.position and self.mcross[0] > 0.0:
            self.buy(size=BUY_SIZE)
        
        if self.position and self.mcross[0] < 0.0:
            self.close()

class RSISMAStrategy(bt.Strategy):
    def __init__(self):
        # self.rsi = bt.talib.RSI(self.data, period=42)
        self.rsi = bt.indicators.RSI(self.data, period=7)
        self.rsidir = self.rsi - self.rsi(-5)

        # self.sma = bt.indicators.SMA(self.data, period=12)
        # self.smadir = self.sma - self.sma(-5)

        self.sma1 = bt.indicators.SMA(self.data, period=5)
        self.sma2 = bt.indicators.SMA(self.data, period=10)
        self.mcross = bt.indicators.CrossOver(self.sma1, self.sma2)

        #bt.indicators.RSI(self.data, period=14)
        #bt.talib.RSI(self.data, period=14)

    def next(self):
        if not self.position:
            # if self.smadir < 0.0 and self.rsi < 35:
            if self.mcross[0] > 0.0 and self.rsi < 30:
            # if self.mcross[0] > 0.0 and self.rsidir < 0.0:
                self.buy(size=BUY_SIZE)
        else:
            # if self.smadir > 0.0 and self.rsi > 65:
            if self.mcross[0] < 0.0 and self.rsi > 70:
            # if self.mcross[0] < 0.0 and self.rsidir > 0.0:
                self.close()

class RSIMACDStrategy(bt.Strategy):
    def __init__(self):
        # self.rsi = bt.talib.RSI(self.data, period=42)
        self.rsi = bt.indicators.RSI(self.data, period=14)
        self.macd = bt.indicators.MACD(self.data,
                                period_me1=5,
                                period_me2=10,
                                period_signal=7)

        self.mcross = bt.indicators.CrossOver(self.macd.macd, self.macd.signal)

    def next(self):
        if not self.position:
            if self.rsi < 30:
                self.buy(size=BUY_SIZE)
        else:
            if self.mcross[0] < 0.0:
                self.close()

class RSIATRStrategy(bt.Strategy):
    def __init__(self):
        # self.rsi = bt.talib.RSI(self.data, period=42)
        self.rsi = bt.indicators.RSI(self.data, period=14)
        self.atr = bt.indicators.ATR(self.data, period=22)

    def next(self):
        if not self.position:
            if self.rsi < 30:
                self.buy(size=BUY_SIZE)
            pdist = self.atr[0] * 3
            self.pstop = self.data.close[0] - pdist
        else:  # in the market
            pclose = self.data.close[0]
            pstop = self.pstop

            if pclose < pstop:
                self.close()  # stop met - get out
            else:
                pdist = self.atr[0] * 3
                # Update only if greater than
                self.pstop = max(pstop, pclose - pdist)