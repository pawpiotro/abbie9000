from bot import TradingBot
from pathlib import Path

# cryptocurrency : one-time buy volume in euros (temp)
TRADE_ASSETS = {
    "BTC":1000,
    "ETH":1000,
    "XLM":1000,
    "LTC":1000,
    "BNB":1000,
    "ADA":1000,
    "DOT":1000
}


def main():
    # save dirs paths
    Path('./logs/').mkdir(parents=True, exist_ok=True)
    
    trading_bots = []

    for curr, quantity in TRADE_ASSETS.items():
        trading_bots.append(TradingBot(curr, quantity))
        try:
            trading_bots[-1].start()
        except Exception as e:
            print("Abbie - Woopsie UwU threamd made a fucky wucky *starts twerking*")
            print(e)

    a = input()
    while a != "quit":
        a = input()

    print("Abbie - closing threads")
    for trading_bot in trading_bots:
        trading_bot.close_ws()
        trading_bot.join()
    
main()
# if __name__ == "__main__":
#     main()
